# Conflict analyses

#### This repository contains a basic guide/instructions to run conflict analyses in phylogenomic data sets.

##### Here you can find examples for [Phyparts](https://bitbucket.org/blackrim/phyparts/src/master/) and [Quartet Sampling](https://github.com/FePhyFoFum/quartetsampling)

There are other tools to explore phylogenetic conflict like [Concordance factors in IQtree](http://www.iqtree.org/doc/Concordance-Factor) or [Quartet Scores](https://github.com/lutteropp/QuartetScores) that you can also give a look.

The example data includes a 41-taxa RAxML tree, the concatenated alignment, and 1242 rooted gene trees from [Amaranthaceae](https://doi.org/10.1093/sysbio/syaa066)  
  

#### Rooting trees

I usually use **pxrr** from [Phyx](https://github.com/FePhyFoFum/phyx) to root trees. pxrr works great, but when having multiple outgroups, it requires that all taxa listed be present in the trees to be rooted, otherwise it produces empty files. If not all taxa listed as outgroup is present, then using pxrr is not that straightforward to use. Also, pxrr cannot handle non-monophyletic or non-graded outgroups. In this case pxrr will output trees rooted with ingroup taxa and will if included in the analyses will produce wrong Phyparts results 

So, to root trees with multiple outgroups that can be all present or not (i.e., from target enrichment data), I use Ya's rooting approach. In the [target enrichment orthology repository](https://bitbucket.org/dfmoralesb/target_enrichment_orthology/src/master/) you can find a script called **root_trees_multiple_outgroups_MO.py**. To use it, you need to clone the repository as the script needs some other libraries. This script uses the same logic of the 'monophyletic outgroup' (MO) ortholog pruning, and checks for duplicated taxa in the outgroups, non-monophyletic/non-graded outgroups, and does not require all outgroups to be present. If you used the lab's ortholog pruning pipeline (e.g., MO pruning), then you should be able to root all your trees at once. If you are working with some other data set tha, you might have the case of duplicated taxa or non-monophyletic/non-graded outgroups. The script will print warnings for those trees and for trees that do not have any outgroups.  

You need to provide a file with the name of the outgroups (one name by line). You can the script as:

	phython root_trees_multiple_outgroups_MO.py inDIR tree_file_ending outDIR outgroup_list_file  
	

#### Running Phyparts

To run Phyparts you need ROOTED gene trees and a ROOTED map tree (e.g. species trees from ASTRAL or concatenated tree from RAxML).

The example data set has a single outgroup, Mecrygen_Mesembryanthemum_crystallinum, and has no missing data

I normally run the full conflict analysis (-a 1). This one provides the information about the frequency of each alternative topology and allow to plot the most frequent alternative (green portion of the pie chart). 

To run the analyses in lab's workstation you call Phyparts directly:

	phyparts -a 1 -m 41_taxa.raxml.tre -d rooted_trees/ -s 50 -o 41_taxa.phyparts_a1_b50
	
If running in other computer you will need to call Phyparts directly from the located folder:

	java -jar -d64 -Xmx60G /target/phyparts-0.0.1-SNAPSHOT-jar-with-dependencies.jar -a 1 -m 41_taxa.raxml.tre -d rooted_trees/ -s 50 -o 41_taxa.phyparts_a1_b50
	
-m is the map tree; -d is the path to the directory of gene trees. The gene trees must be in Newick format and the folder should not contain any other file besides the tree files; -s is the node support threshold. Nodes below this treshold will not included in the conflict analyses and labeled as uninformative (gray portion in the pie chart). I usually use 50% bootstrap support as threshold. -0 is the prefix for the output file.  


##### Phyparts will produced four output files:

- **41_taxa.phyparts_a1_b50.concon.tre** (Main output file. The first tree is the number of gene trees supporting each node. The second tree is the number of gene trees conflicting with each node. The third tree is the ICA score for each node)

The other output files contain information about topology frequency that is mainly used for visualization.

- 41_taxa.phyparts_a1_b50.hist  
- 41_taxa.phyparts_a1_b50.hist.alts  
- 41_taxa.phyparts_a1_b50.node.key  


#### Visualizing Phyparts results

A great way to visualize results from Phyparts is with pie charts. Matt Johnson has a visualization script [here](https://github.com/mossmatters/MJPythonNotebooks/blob/master/PhyParts_PieCharts.ipynb)

To plot the tree with the pie charts and the number of concordant and discordant gene trees you need the map tree and the output files from Phyparts. The input arguments are the map tree, the Phyparts output prefix, the total number of gene trees, and the name of the SVG output file 
 
	python MJPythonNotebooks/phypartspiecharts.py 41_taxa.raxml.tre 41_taxa.phyparts_a1 1242 --svg_name 41_taxa.phyparts_a1.svg
 
The output figure should look like this

![bs](/test_data/phyparts/41_taxa.phyparts_a1_b50.svg)

Pie charts represent the proportion of gene trees that support that node (blue), the proportion that support the main alternative bifurcation (green), the proportion that support the remaining alternatives (red), and the proportion (conflict or support) that have < 50% bootstrap support (gray).

As a rule of thumb, if you ran correctly Phyparts will all rooted trees, the root node should be all gray.

If you ran Phyparts without node support threshold (-s), you won't have gray portion and the root node should be all blue.

![no_bs](/test_data/phyparts/41_taxa.phyparts_a1.svg)  


#### Visualizing Phyparts results when expecting missing data (e.g. Target enrichment data sets)

The original pie chart scrip from Matt Johnson assumes no missing data and fix number of gene trees for nodes to draw the pie charts. When using this script and there are missing data in the gene trees, the gray portion of the pie charts has a different meaning. 

- If no node support filtering(-s) was used, the gray part represents the proportion of missing data.

- If node support filtering (e.g. -s 50) was used, the gray part represents proportion of gene tres with <50% bootstrap support + the proportion of missing data. Be specially carefull of this case as this will inflate the gray portion of the pie chart and give the false impression a lot of uninformative gene trees in your dataset.

##### I have modified the plotting scripts to plot piecharts that are proportional to the number of informing gene trees per node or piecharts that plot separately the uninformative and missing data using a fix number of total of gene trees. You can find script and detail instructions [here](https://bitbucket.org/dfmoralesb/target_enrichment_orthology/src/master/)  

##### Pie chart examples (using node suport filtering)  

- Pie charts made with the original script. Gray = missing data + uninformative

![original](/test_data/other_pie_charts/pies_original_fixed_number.svg) 

- Pie charts made with 'phypartspiecharts_proportional.py'  Dark gray = uninformative; Light gray = missing data

![original](/test_data/other_pie_charts/pies_original_missing_data.svg) 

- Pie charts made with 'phypartspiecharts_proportional.py'  Gray = only uninformative.

![original](/test_data/other_pie_charts/pies_original_proportional.svg)  


#### Running Quartet Sampling

To run Quartet Sampling you need a map tree (e.g. species trees from ASTRAL or concatenated tree from RAxML), a concatenated alignment, and a partition file in RAxML format (optional)

I normally run Quartet Sampling without any partition or by gene tree (i.e. the whole alignment is treated as one partition for all replicates). 

	python3.8 /home/diegomorales_briones/Apps/quartetsampling/pysrc/quartet_sampling.py --tree 41_taxa.raxml.tre --align 41_taxa.aln.phy --reps 1000 --threads 24 --results-dir QS_41_taxa.raxml --engine raxml --data-type nuc
	
The arguments are: map tree (--tree), alignment (--align), number of replicates (--reps), the number of processors (--threads), output directory (--results-dir), software to estimate ML (--engine), alignment data type (--data-type) 

I use 1000 replicates and RAxML to get the ML of the quartets. You can also use RAxML-ng, PAUP or IQtree. The alignment need to be in 'relaxes phylip' format

##### Quartet Sampling will produced the following output files:


- **RESULT.labeled.tre.figtree.** A [FigTree](http://tree.bio.ed.ac.uk/software/figtree/) format phylogeny that contains all QS scoresand a “score” field with QC/QD/QI for internal branches. This is the main file that I check and the one I used to make figure using the 'score' filed.


- **RESULT.node.scores.csv.** This file allows you to check at a node which discordant option is more common in cases where QD is low, indicating higher presence of one discordant option. A comma-separated values (CSV) file with:

	•node_label: The label of the internal branch (QS###) or terminal branch (original label)  
	
	•freq0: The number of concordant replicates over the non-uncertain total  
	
	•qc: The Quartet Concordance score (internal branches only; measures frequency of concor-dant over discordant)  
	
	•qd: The Quartet Differential score (internal branches only; measures skew in the two discor-dant tree counts)  
	
	•qi: The Quartet Informativeness score (internal branches only; measures number of replicatesthat fail likelihood cutoff)  
	
	•qf: The Quartet Fidelity score (terminal branches only; measures the number of replicates forwhich this branch produced concordant quartets)  
	
	•diff: Example likelihood differential from last replicate (used for diagnostic purposes)  
	
	•num_replicates:  Number of replicates actually sampled per branch (may not equal the num-ber specified by-N for internal branches when fewer replicates are possible, and will not equal that number for terminal branches)  
	
	•notes:  Additional notes (enables user to add custom notes to their output after running; e.g.,labeling key branches)  
	

- **RESULT.node.counts.csv.** A tab-separated values (TSV) file with:  

	•node_label: The label of the internal branch (QS###) or terminal branch (original label)  
	
	•count0: The count of the number of QS replicates for the concordant quartet arrangement  
	
	•count1: The count of the number of QS replicates for one of the discordant quartet arrangements  
	
	•count2: The count of the number of QS replicates for the other discordant quartet arrangement  
	
	•topo0, topo1, and topo2: provide example quartet trees (in parenthetical notation) showingthe arragement of a representative quartet of taxa spanning the node for the arragements corresponding to the counts  
	
	
- **RESULT.labeled.tre.** A Newick tree with each internal branch labeled with their QS## identifier.  


- **RESULT.labeled.tre.freq/qc/qd/qu.** A Newick tree with the each internal branch labeled with frequency of concordant replicates or QC/QD/QI scores.  


#### Visualizing Quartet Sampling results:

I use FigTree to open the RESULT.labeled.tre.figtree and show the QS score as branch labels. By default the branch labels are colored by support. 

![no_partition](/test_data/quartet_sampling/QS_41_taxa.raxml/RESULT.labeled.tre.figtree.png)

If running Quartet Sampling using the **--genetrees** option. The partition file will be used to divide the alignment into separate gene trees regions. Gene trees alignments will be sampled random for the quartet replicates. As seen below, the scores tend to be slightly lower mostly because the number of informative number of replicates (QI) tend to be significantly lower. 
 
![genetrees](/test_data/quartet_sampling/QS_41_taxa_genetrees.raxml/RESULT.labeled.tre.figtree.png)

If want to plot figures similar to the ones in the paper you can use the following R code.  


	###QC with circles###
	
	qc <- read.tree("RESULT.labeled.tre.qc")
	qc$node.label <- gsub("qc=","",qc$node.label)
	qc$node.label
	
	co <- c("chartreuse4", "chartreuse", "darkorange", "brown1")
	p <- character(length(qc$node.label))
	p[as.numeric(qc$node.label) > 0.25] <- co[1]
	p[as.numeric(qc$node.label) <= 0.25 & as.numeric(qc$node.label) > 0] <- co[2]
	p[as.numeric(qc$node.label) <= 0 & as.numeric(qc$node.label) >= "-0.05"] <- co[3]
	p[as.numeric(qc$node.label) < "-0.05"] <- co[4]
	
	
	plot(ladderize(qc, right = F), cex = 0.3, no.margin=T, label.offset=0.0005, use.edge.length = FALSE, node.depth=2)
	for(j in 1:Nnode(qc)) 
	{
 	  #if(qc$node.label[[j]] != "" & as.numeric(qc$node.label[[j]])) 
 	  if(qc$node.label[[j]] != "") #this plots "zero"
	  {
	    nodelabels("", j+length(qc$tip.label), cex=0.75, bg=p[j], pch=21, frame="n")
	  }
	}
	
	legend("topleft",inset = c(0,0.01), legend=c("QC > 0.25", "0.25 >= QC > 0", " 0 >= QC > = -0.05", "QC <= -0.05"), pt.bg=c("chartreuse4", "chartreuse", "darkorange", "brown1"), pch=c(21,21,21,21), cex=0.5,bty = "n")
	
	
	###Plot QC/QD/QI as node labels###
	
	
	qc <- read.tree("RESULT.labeled.tre.qc")
	qd <- read.tree("RESULT.labeled.tre.qd")
	qi <- read.tree("RESULT.labeled.tre.qi")
	
	
	qc$node.label <- gsub("qc=","",qc$node.label)
	qd$node.label <- gsub("qd=","",qd$node.label)
	qi$node.label <- gsub("qi=","",qi$node.label)
	
	qc$node.label[qc$node.label == "NA"] <- "-"
	qd$node.label[qd$node.label == "NA"] <- "-"
	qi$node.label[qi$node.label == "NA"] <- "-"
	
	qc$node.label <-paste(qc$node.label, qd$node.label, qi$node.label, sep="/")
	qc$node.label[qc$node.label == "//"] <- ""
	qc$node.label
	
	nodelabels(qc$node.label, adj=c(1.3, -0.5), frame="n", cex=0.3, font=1)  
	
	
The resulting figures will be:

![no_partition](/test_data/quartet_sampling/QS_41_taxa.raxml/QS.pdf.svg)  


![genetrees](/test_data/quartet_sampling/QS_41_taxa_genetrees.raxml/QS_genetree.pdf.svg)




	
